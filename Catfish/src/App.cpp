#include "Window.h"

int main(){
    Window window(1280, 720, "Catfish - The First Adventure");
    
    while(!window.ShouldClose()){
        window.SwapBuffers();
    }
}